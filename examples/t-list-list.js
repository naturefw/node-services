/**
 * 不使用事务，测试获取一条记录
 */

console.log('测试 MySQL 基础操作----')
// 引入help
const {
  MySQLHelp,
  getModel,
  getList
 } = require('../packages/mysql.js')

// 配置信息
const config = require('../public/config/mysql-test.json')

// 实例
const help = new MySQLHelp(config)

// meta
const meta = require('../public/model/10010-user.json')
const meta2 = require('../public/model/10020-test.json')


// 获取全部数据的测试
const test_getAllData = () => {
  console.log('')
  console.log('---- getList ----')
  return new Promise((resolve, reject) => {
    // 查询条件
    const query = {
      id: [417, [70, 86]]
    }
    
    getList(help, info, query).then((list) => {
      // 添加成功
      console.log('外部获取数据 ———— 多条:', list)
      resolve()

    })
  })
}
  

// 开始测试
test_getAllData().then(() => {
 
  // 关闭
  help.close()
})
 