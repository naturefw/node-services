/**
 * model 的添加、修改、删除、获取
 * 不使用事务
 */


console.log('测试 MySQL 基础操作----')
// 引入help
const {
  MySQLHelp,
  addModel,
  updateModel,
  deleteModel,
  getModel
} = require('../packages/mysql.js')

// 配置信息
const config = require('../public/config/mysql-test.json')

// 实例
const help = new MySQLHelp(config)

// meta
const meta = require('../public/model/10010-user.json')
const meta2 = require('../public/model/10020-test.json')

let newDataId = 0

// 添加数据的测试
const test_addData = (number) => {
  console.log('\n---- addModel ----')
  return new Promise((resolve, reject) => {
    // 要添加的数据
    const model = {
      name: '测试基础添加数据-A-11',
      age: number
    }

    // meta.tableName = 'a'
    addModel(help, meta, model).then((newId) => {
      // 添加成功
      console.log('外部添加新数据:', newId)
      newDataId = newId
      resolve()
    })
  })
}


// 修改数据的测试
const test_updateData = () => {
  console.log('\n---- updateModel ----')
  return new Promise((resolve, reject) => {
    // 要修改的数据
    const model = {
      name: '测试基础添加数据-A-11--修改了',
      age: 300
    }

    // meta.tableName = 'a'
    updateModel(help, meta, model, newDataId).then((count) => {
      // 添加成功
      console.log('修改数据，影响行数:', count)
      resolve()
    })
  })
}

// 删除数据的测试
const test_deleteData = () => {
  console.log('\n---- deleteModel ----')
  return new Promise((resolve, reject) => {
    // meta.tableName = 'a'
    deleteModel(help, meta, newDataId).then((count) => {
      // 添加成功
      console.log('删除数据，影响行数:', count)
      resolve()
    })
  })
}


// 获取数据的测试
const test_getData = () => {
  console.log('\n---- updateModel ----')
  return new Promise((resolve, reject) => {
    // meta.tableName = 'a'
    getModel(help, meta2, 1).then((model) => {
      // 添加成功
      console.log('获取model:', model)
      resolve(model)
    })
  })
}

test_getData()

// 开始测试

// 循环添加
for (let i=100; i<110;i++) {
  // test_addData(i)
}
// 依次添加、修改、删除
// test_addData('66')

for (let i=200; i<210;i++) {
  // test_addData(i)
}

// 依次操作
/*
test_addData('51').then(() => {
  // 修改
  return test_updateData()
}).then(() => {
  // 获取
  return test_getData()
}).then(() => {
  // 删除
  return test_deleteData()
}).then(() => {
  help.close()
})


// 依次操作，并且验证
/*
test_addData('51').then(() => {
  // 获取记录，验证数据
  return test_getData()
}).then((model) => {
  // 验证数据
  console.log('（添加）name ：', model[0].name === '测试基础添加数据-A-11')
  console.log('（添加）age ：', model[0].age === 51)
  // 修改
  return test_updateData()
}).then(() => {
  // 获取记录，验证数据
  return test_getData()
}).then((model) => {
  // 验证数据
  console.log('（修改）name ：', model[0].name === '测试基础添加数据-A-11--修改了')
  console.log('（修改）age ：', model[0].age === 300)
  // 删除
  return test_deleteData()
}).then(() => {
  // 获取记录，验证数据
  return test_getData()
}).then((model) => {
  // 验证数据
  console.log('删除！', model.length === 0)
  help.close()
})
*/

// await 模式
/*
async function check() {
  // 添加数据
  await test_addData('51')
  // 获取记录进行验证
  const model1 = await test_getData()
  console.log('（await 添加）name ：', model1[0].name === '测试基础添加数据-A-11')
  console.log('（await 添加）age ：', model1[0].age === 51)

  // 修改数据
  await test_updateData()
  // 获取记录进行验证
  const model2 = await test_getData()
  console.log('（await 修改）name ：', model2[0].name === '测试基础添加数据-A-11--修改了')
  console.log('（await 修改）age ：', model2[0].age === 300)

  // 修改数据
  await test_deleteData()
  // 获取记录进行验证
  const model3 = await test_getData()
  // 验证数据
  console.log('删除！', model3.length === 0)

  help.close()

}

// 开始验证
// check()
*/