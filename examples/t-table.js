/**
 * 建表语句
 */


 console.log('测试 MySQL 建表操作----')
 // 引入help
 const {
   MySQLHelp 
 } = require('../packages/mysql.js')
 
 // 配置信息
 const config = require('../public/config/mysql-test.json')
 
 // 实例
 const help = new MySQLHelp(config)
 
 const sql = `
 CREATE TABLE \`test\`.\`t-int111\` (
    \`id\` INT UNSIGNED ZEROFILL NOT NULL COMMENT '主键',
    \`t-BIGINT\` BIGINT(12) UNSIGNED ZEROFILL NOT NULL  COMMENT '姓名',
    \`t-DECIMAL\` DECIMAL NULL  COMMENT '姓名222a',
    PRIMARY KEY (\`id\`),
    UNIQUE INDEX \`t-BIGINT_UNIQUE\` (\`t-BIGINT\` ASC) VISIBLE)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci
  COMMENT = '测试数字类的字段';
 `

 help.query(sql).then((res) => {
  console.log(res)
 })

