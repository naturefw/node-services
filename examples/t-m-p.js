/**
 * model 的添加、修改、删除、获取
 * 使用事务
 */


console.log('测试 MySQL 基础操作 使用事务----')
// 引入help
const {
  MySQLHelp,
  addModel,
  updateModel,
  deleteModel,
  getModel
} = require('../packages/mysql.js')

// 配置信息
const config = require('../public/config/mysql-test.json')

// 实例
const help = new MySQLHelp(config)

// meta
const meta = require('../public/model/10010-user.json')

let newDataId = 0

// 添加数据的测试
const test_addData = (number, cn) => {
  console.log('\n---- addModel ----')
  return new Promise((resolve, reject) => {
    // 要添加的数据
    const model = {
      name: '测试基础添加数据-A-11',
      age: number
    }

    // meta.tableName = 'a'
    addModel(help, meta, model, cn).then((newId) => {
      // 添加成功
      console.log('外部添加新数据:', newId)
      newDataId = newId
      resolve(newId)
    })
  })
}


// 修改数据的测试
const test_updateData = (cn) => {
  console.log('\n---- updateModel ----')
  return new Promise((resolve, reject) => {
    // 要修改的数据
    const model = {
      name: '测试基础添加数据-A-11--修改了',
      age: 300
    }

    // meta.tableName = 'a'
    updateModel(help, meta, model, newDataId, cn).then((count) => {
      // 添加成功
      console.log('修改数据，影响行数:', count)
      resolve()
    })
  })
}

// 删除数据的测试
const test_deleteData = (cn) => {
  console.log('\n---- deleteModel ----')
  return new Promise((resolve, reject) => {
    // meta.tableName = 'a'
    deleteModel(help, meta, newDataId, cn).then((count) => {
      // 添加成功
      console.log('删除数据，影响行数:', count)
      resolve()
    })
  })
}


// 获取数据的测试
const test_getData = (cn) => {
  console.log('\n---- updateModel ----')
  return new Promise((resolve, reject) => {
    // meta.tableName = 'a'
    getModel(help, meta, newDataId, cn).then((model) => {
      // 添加成功
      console.log('获取model:', model)
      resolve(model)
    })
  })
}

// 开始测试

// 循环添加
help.begin().then((cn) => {
  const arr = []
  for (let i=100; i<110;i++) {
    arr.push(test_addData(i, cn))
  }

  Promise.all(arr).then((res) => {
    console.log('事务的批量添加：', res)
    // 提交事务
    help.commit(cn).then(() => {
      console.log(' +  +  + promise 提交事务的回调:')
      // 关闭
      help.close(cn)
      // help.closePool()
    })
  })

})
 

// 循环添加
help.begin().then((cn) => {
  const arr = []
  for (let i=200; i<210;i++) {
    arr.push(test_addData(i, cn))
  }

  Promise.all(arr).then((res) => {
    console.log('事务的批量添加：', res)
    // 提交事务
    help.commit(cn).then(() => {
      console.log(' +  +  + promise 提交事务的回调:')
      // 关闭
      help.close(cn)
      // help.closePool()
    })
  })

})
 

// await 模式

async function check() {
  const cn = await help.begin()

  // 添加数据
  await test_addData('51', cn)
  // 获取记录进行验证
  const model1 = await test_getData(cn)
  console.log('（await 添加）name ：', model1[0].name === '测试基础添加数据-A-11')
  console.log('（await 添加）age ：', model1[0].age === 51)

  // 修改数据
  await test_updateData(cn)
  // 获取记录进行验证
  const model2 = await test_getData(cn)
  console.log('（await 修改）name ：', model2[0].name === '测试基础添加数据-A-11--修改了')
  console.log('（await 修改）age ：', model2[0].age === 300)

  // 修改数据
  await test_deleteData(cn)
  // 获取记录进行验证
  const model3 = await test_getData(cn)
  // 验证数据
  console.log('删除！', model3.length === 0)

  await help.commit(cn)
  help.close(cn)
  // help.closePool()

}

// 开始验证
check()

 