const PoolConnection = {
    _events:  { //[Object: null prototype]
      end: [Function, _removeFromPool],
      error: [Function (anonymous)]
    },
    _eventsCount: 2,
    _maxListeners: undefined,
    config:  { //ConnectionConfig
      host: 'localhost',
      port: '3306',
      localAddress: undefined,
      socketPath: undefined,
      user: 'root',
      password: 'admin',
      database: 'test',
      connectTimeout: 10000,
      insecureAuth: false,
      supportBigNumbers: false,
      bigNumberStrings: false,
      dateStrings: false,
      debug: undefined,
      trace: true,
      stringifyObjects: false,
      timezone: 'local',
      flags: '',
      queryFormat: undefined,
      pool:  { //Pool
        _events: 1, // [Object: null prototype] {},
        _eventsCount: 0,
        _maxListeners: undefined,
        config: [PoolConfig],
        _acquiringConnections: [],
        _allConnections: [Array],
        _freeConnections: [],
        _connectionQueue: [],
        _closed: false,
        [Symbol(kCapture)]: false
      },
      ssl: false,
      localInfile: true,
      multipleStatements: false,
      typeCast: true,
      maxPacketSize: 0,
      charsetNumber: 33,
      clientFlags: 455631,
      protocol41: true
    },
    _socket: { //<ref *1> Socket 
      connecting: false,
      _hadError: false,
      _parent: null,
      _host: 'localhost',
      _readableState:  { //ReadableState
        objectMode: false,
        highWaterMark: 16384,
        buffer: 1, //BufferList { head: null, tail: null, length: 0 },
        length: 0,
        pipes: [],
        flowing: true,
        ended: false,
        endEmitted: false,
        reading: false,
        sync: false,
        needReadable: true,
        emittedReadable: false,
        readableListening: false,
        resumeScheduled: false,
        errorEmitted: false,
        emitClose: false,
        autoDestroy: false,
        destroyed: false,
        errored: null,
        closed: false,
        closeEmitted: false,
        defaultEncoding: 'utf8',
        awaitDrainWriters: null,
        multiAwaitDrain: false,
        readingMore: false,
        decoder: null,
        encoding: null,
        [Symbol(kPaused)]: false
      },
      _events:  { //[Object: null prototype]
        end: [Array],
        data: [Function (anonymous)],
        error: [Function, bound ],
        connect: [Function, bound ]
      },
      _eventsCount: 4,
      _maxListeners: undefined,
      _writableState:  { //WritableState
        objectMode: false,
        highWaterMark: 16384,
        finalCalled: false,
        needDrain: false,
        ending: false,
        ended: false,
        finished: false,
        destroyed: false,
        decodeStrings: false,
        defaultEncoding: 'utf8',
        length: 0,
        writing: false,
        corked: 0,
        sync: false,
        bufferProcessing: false,
        onwrite:1,// [Function: bound onwrite],
        writecb: null,
        writelen: 0,
        afterWriteTickInfo: null,
        buffered: [],
        bufferedIndex: 0,
        allBuffers: true,
        allNoop: true,
        pendingcb: 0,
        prefinished: false,
        errorEmitted: false,
        emitClose: false,
        autoDestroy: false,
        errored: null,
        closed: false,
        closeEmitted: false,
        writable: true
      },
      allowHalfOpen: false,
      _sockname: null,
      _pendingData: null,
      _pendingEncoding: '',
      server: null,
      _server: null,
      timeout: 0,
      [Symbol(async_id_symbol)]: 5,
      [Symbol(kHandle)]:  { //TCP
        reading: true,
        onconnection: null,
        [Symbol(owner_symbol)]: [Circular *1]
      },
      [Symbol(kSetNoDelay)]: false,
      [Symbol(lastWriteQueueSize)]: 0,
      [Symbol(timeout)]:  { //Timeout
        _idleTimeout: -1,
        _idlePrev: null,
        _idleNext: null,
        _idleStart: 314,
        _onTimeout: null,
        _timerArgs: undefined,
        _repeat: null,
        _destroyed: true,
        [Symbol(refed)]: false,
        [Symbol(kHasPrimitive)]: false,
        [Symbol(asyncId)]: 9,
        [Symbol(triggerId)]: 1
      },
      [Symbol(kBuffer)]: null,
      [Symbol(kBufferCb)]: null,
      [Symbol(kBufferGen)]: null,
      [Symbol(kCapture)]: false,
      [Symbol(kBytesRead)]: 0,
      [Symbol(kBytesWritten)]: 0
    },
    _protocol:  { //Protocol
      _events:  { //[Object: null prototype]
        data: [Function (anonymous)],
        end: [Array],
        handshake: [Function, bound, _handleProtocolHandshake],
        initialize: [Function, bound, _handleProtocolInitialize],
        unhandledError: [Function, bound ],
        drain: [Function, bound ],
        enqueue: [Function, bound, _handleProtocolEnqueue]
      },
      _eventsCount: 7,
      _maxListeners: undefined,
      readable: true,
      writable: true,
      _config:  { //ConnectionConfig
        host: 'localhost',
        port: '3306',
        localAddress: undefined,
        socketPath: undefined,
        user: 'root',
        password: 'admin',
        database: 'test',
        connectTimeout: 10000,
        insecureAuth: false,
        supportBigNumbers: false,
        bigNumberStrings: false,
        dateStrings: false,
        debug: undefined,
        trace: true,
        stringifyObjects: false,
        timezone: 'local',
        flags: '',
        queryFormat: undefined,
        pool: [Pool],
        ssl: false,
        localInfile: true,
        multipleStatements: false,
        typeCast: true,
        maxPacketSize: 0,
        charsetNumber: 33,
        clientFlags: 455631,
        protocol41: true
      },
      _connection: [Circular *2],
      _callback: null,
      _fatalError: null,
      _quitSequence: null,
      _handshake: true,
      _handshaked: false,
      _ended: false,
      _destroyed: false,
      _queue: [ [Handshake] ],
      _handshakeInitializationPacket:  { //HandshakeInitializationPacket
        protocolVersion: 10,
        serverVersion: '8.0.15',
        threadId: 71,
        scrambleBuff1: 1,//<Buffer 22 42 43 66 7e 7a 51 3d>,
        filler1:  1,//<Buffer 00>,
        serverCapabilities1: 65535,
        serverLanguage: 255,
        serverStatus: 2,
        serverCapabilities2: 50175,
        scrambleLength: 21,
        filler2: 1,// <Buffer 00 00 00 00 00 00 00 00 00 00>,
        scrambleBuff2: 1,// <Buffer 25 17 28 44 0a 2b 42 5e 5f 03 08 42>,
        filler3:  1,//<Buffer 00>,
        pluginData: 'caching_sha2_password',
        protocol41: true
      },
      _parser:  {//Parser
        _supportBigNumbers: false,
        _buffer: 1,// <Buffer 07 00 00 02 00 00 00 02 00 00 00>,
        _nextBuffers: [BufferList],
        _longPacketBuffers: [BufferList],
        _offset: 11,
        _packetEnd: 11,
        _packetHeader: [PacketHeader],
        _packetOffset: 4,
        _onError: [Function, bound, handleParserError],
        _onPacket: [Function, bound ],
        _nextPacketNumber: 3,
        _encoding: 'utf-8',
        _paused: false
      },
      [Symbol(kCapture)]: false
    },
    _connectCalled: true,
    state: 'connected',
    threadId: 71,
    _pool:  {//Pool
      _events:  {}, //[Object, null, prototype]
      _eventsCount: 0,
      _maxListeners: undefined,
      config:  { // PoolConfig
        acquireTimeout: 10000,
        connectionConfig: [ConnectionConfig],
        waitForConnections: true,
        connectionLimit: 10,
        queueLimit: 0
      },
      _acquiringConnections: [],
      _allConnections: [ [Circular *2] ],
      _freeConnections: [],
      _connectionQueue: [],
      _closed: false,
      [Symbol(kCapture)]: false
    },
    [Symbol(kCapture)]: false
  }