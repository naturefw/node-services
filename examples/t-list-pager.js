/**
 * 不使用事务，测试分页
 */

console.log('测试 MySQL 分页 ----')
// 引入help
const {
  MySQLHelp,
  getCount,
  getPager
 } = require('../packages/mysql.js')
// 配置信息
const config = require('../public/config/mysql-test.json')

// 实例
const help = new MySQLHelp(config)

// meta
const meta = require('../public/model/10050-user-find.json')

const pager = meta.pager

// 查询条件
const query = {
  name: [403, 'a']
}

// 各种函数

// 获取分页数据
const test_getPager = () => {
  console.log('\n---- getPager ----')
  return new Promise((resolve, reject) => {
    
    getPager(help, meta, query, pager).then((list) => {
      // 添加成功
      console.log('外部获取数据 ———— 分页:', list)
      resolve(list)
    })
  })
}

// 获取总记录数
const test_getCount = () => {
  console.log('\n---- getCount ----')
  return new Promise((resolve, reject) => {
    getCount(help, meta, query).then((count) => {
      // 添加成功
      console.log('外部获取总记录数:', count)
      resolve(count)
    })
  })
}
  

const test = async (canCount) => {
    if (canCount) {
      pager.pageTotal = await test_getCount()
    }
    
    // 开始测试
    test_getPager().then((list) => {
      // 关闭
      help.close()
    })
}


test(true)