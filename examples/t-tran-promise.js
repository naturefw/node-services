/**
 * 测试，使用事务的MySQL的访问
 * primose 的形式
 */

// 引入help
const {
  MySQLHelp,
  addModel
 } = require('../packages/mysql.js')

// 配置信息
const config = {
  database: 'test',
  user: 'root',
  password: 'admin',
  host: 'localhost', 
  port: '3306'
}
// 实例，使用事务
const help = new MySQLHelp(config, true)

// ---------------------
console.log('')
console.log('---- addData ----')

const info = {
  tableName: 'aaa',
  cols: {
    aaacol: 'json'
  } 
}
const values = {
  aaacol: {
    name: '第一轮事务111'
  }
}

// 开启事务
help.begin().then((cn) => {
  values.aaacol.name = '第一轮事务111'
  addData(help, info, values, cn).then((newId) => {
    // 添加成功
    console.log('第一轮事务111:', newId)
    // 下一个操作
    values.aaacol.name = '第一轮事务111222'
    return addData(help, info, values, cn)
  
  }).then((newId) => {
    console.log('第一轮事务222:', newId)
    values.aaacol.name = '第一轮事务111222333'
    // 测试回滚，改表名应该报错回滚
    // info.tableName = 'wwww'
    // 下一个操作
    return addData(help, info, values, cn)
  }).then((newId) => {
    console.log('第一轮事务333:', newId)
    // 读取记录
    const sql = 'select * from aaa order by id desc limit 10 '
   
    console.log('第一轮事务获取记录:')
    help.query(sql, [], cn).then((res) => {
      console.log('第一轮事务获取记录:', res)
    })
    
    // 提交事务
    help.commit(cn).then(() => {
      // 关闭
      help.close(cn)
      help.closePool()
    })
  })
})

// 再次开启开启事务
/*
help.begin().then(() => {
  values.aaacol.name = '再次开启事务aa'
  addData(help, info, values, cn).then((newId) => {
    // 添加成功
    console.log('第二轮事务111:', newId)
    // 下一个操作
    values.aaacol.name = '再次开启事务aabb'
    // 提交事务
    help.commit().then(() => {})

    return addData(help, info, values, cn)
  
  }).then((newId) => {
    console.log('第二轮事务222:', newId)
    // 下一个操作
    values.aaacol.name = '再次开启事务aabbcc'
    return addData(help, info, values, cn)
  }).then((newId) => {
    console.log('第二轮事务333:', newId)
    // 读取记录
    const sql = 'select * from aaa order by id desc'
    // 提交事务
    help.commit().then(() => {
      console.log('第二轮事务获取记录:')
      help.query(sql).then((res) => {
        console.log('第二轮事务获取记录 :', res)
      })
    })
  })
})

*/