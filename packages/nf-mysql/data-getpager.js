
/**
 * 分页获取数据，可以查询
 * @param { MySQLHelp } help 访问数据库的实例
 * @param { Object } info 表、字段
 * @param { Object } query 查询条件
 * @param { Object } pager 数据
 * @returns 添加记录的ID
 * * info 结构：
 * * * tableName： '', 表名
 * * * cols：{colName: '类型'}, 需要显示的字段
 * * query 结构（查询条件）：
 * * * { colName: [401, 11] } 字段名称，查询方式，查询关键字
 * * pager 结构：
 * * * pageSize: 20 // 一页显示多少条记录
 * * * orderBy: { id: false } // 排序字段：字段名，false表示倒序。
 * * * pageTotal: 100 // 符合查询条件的总记录数 
 * * * pageIndex: 1 // 显示第几页的记录，从 1 开始
 */
async function getPager(help, info, query, pager) {
  console.log('开始分页 :')
  const myPromise = new Promise((resolve, reject) => {
    // 查询条件和查询参数
    const { whereQuery, whereValue } = help._getWhereQuery(query)
    // 设置排序和分页
    const { orderBy, limit } = help._getPager(pager)

    // 设置显示的字段
    const showCol = Object.keys(info.cols)
    if (showCol.length === 0 ) {showCol.push('*')}

    // 拼接查询语句
    const sql = `SELECT ${showCol.join(',')} FROM ${info.tableName} ${whereQuery} ${orderBy} ${limit}`
    console.log('select-sql:', sql, whereValue)

    help.query(sql, whereValue)
      .then((res) => {
        // 添加成功
        // console.log('分页获取记录:', res)
        resolve(res)
      })
      .catch((err) => {
        // 出错了
        console.log('分页获取记录失败了:', err)
        reject(err)
      })
  })
  
  return myPromise
}


function getCount(help, info, query) {
  return new Promise((resolve, reject) => {
    // 查询条件和查询参数
    const { whereQuery, whereValue } = help._getWhereQuery(query)
    // 统计总数
    const sql = `SELECT count(1) as count FROM ${info.tableName}  ${whereQuery} `
    console.log('count-sql:', sql, whereValue)
    help.query(sql, whereValue).then((re) => {
      resolve(re[0].count)
    }).catch((err) => {
      // 出错了
      console.log('统计总记录数失败了:', err)
      reject(err)
    })
  })
}

module.exports = {
  getCount,
  getPager
}