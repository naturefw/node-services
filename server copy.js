﻿// 引入服务
const http = require('http')

http.createServer((req, res) => {
  // 设置响应头
  res.writeHeader(200, {
    "Content-Type" : "application/json"
  })
  // response.setHeader('Content-Type', 'application/json')
  console.log('有访问者')
  // 输出开头
  res.write('{"code":"200" ')
  
  // 解构获取 URL、请求方法和头部对象
  const { url, method, headers } = req

  const re = {
    url,
    method,
    headers
  }
  // 输出 head
  res.write(', "head":' + JSON.stringify(re, null, 2))
  
  // 请求体
  let body = ''
  req.on('data', chunk => {
    body += chunk
  })
  req.on('end', () => {
    // 输出body
    res.write(`, "body":${body}`)
    // 输出结束标识
    res.write('}')
    res.end()
  })
  
})
// 设置监听端口为 6000
.listen(6000, () => {
  console.log('服务已经开启：http://localhost:6000')
})
