// 实现添加服务

// 引入help
const { getList } = require('../../packages/mysql.js')

console.log('\n getPager 文件被加载\n')

/**
 * 实现获取记录服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} serviceInfo 服务的 meta
 * @param {object} query 查询条件
 * @returns 返回新添加的记录的ID
 */
const getAll1 = (userInfo, help, serviceInfo, query) => {
  return new Promise((resolve, reject) => {
    // 加载meta
    modelName = serviceInfo.model
    const info = require(`../../public/model/${modelName}.json`)

    console.log('\n启动 getAll 服务\n', info)
    
    console.log('查询条件:', query)

    getList(help, info, query).then((list) => {
      // 添加成功
      console.log('获取数据列表:', list)
      resolve({ list })
    }).catch((err) => {
      reject('获取数据列表出错！')
    })

  })
}

module.exports = getAll1
