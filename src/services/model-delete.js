// 引入help
const { deleteModel } = require('../../packages/mysql.js')

console.log('\n★ update 文件被加载\n')

/**
 * 实现删除服务
 * @param {object} userInfo 当前登录人的信息
 * @param {object} help 访问数据库的实例
 * @param {objec} serviceInfo 服务的 meta
 * @param {object} model 占位用
 * @param {number|string} id 记录ID
 * @returns 返回影响的行数
 */
const del = (userInfo, help, serviceInfo, model, id) => {
  return new Promise((resolve, reject) => {
    // 加载meta
    modelName = serviceInfo.model
    const info = require(`../../public/model/${modelName}.json`)

    console.log('\n启动 delete 服务\n')
    deleteModel(help, info, id).then((count) => {
      console.log('删除数据，影响行数:', count)
      resolve({ count })
    }).catch((err) => {
      reject('删除数据出错！')
    })

  })
}

module.exports = del
